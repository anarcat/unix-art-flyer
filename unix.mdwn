The Unix Philosophy
===================

The "Unix philosophy" originated with Ken Thompson's early meditations on how to design a small but capable operating system with a clean service interface. It grew as the Unix culture learned things about how to get maximum leverage out of Thompson's design. It absorbed lessons from many sources along the way.

The Unix philosophy is not a formal design method. It wasn't handed down from the high fastnesses of theoretical computer science as a way to produce theoretically perfect software. Nor is it that perennial executive's mirage, some way to magically extract innovative but reliable software on too short a deadline from unmotivated, badly managed, and underpaid programmers.

The Unix philosophy (like successful folk traditions in other engineering disciplines) is bottom-up, not top-down. It is pragmatic and grounded in experience. It is not to be found in official methods and standards, but rather in the implicit half-reflexive knowledge, the expertise that the Unix culture transmits. It encourages a sense of proportion and skepticism - and shows both by having a sense of (often subversive) humor.

 1. **Modularity**: Write simple parts connected by clean interfaces.
 2. **Clarity**: Clarity is better than cleverness.
 3. **Composition**: Design programs to be connected to other programs.
 4. **Separation**: Separate policy from mechanism, interfaces from engines.
 5. **Simplicity**: Design for simplicity; add complexity only where you must.
 6. **Parsimony**: Write big programs only when it is clear that nothing else will do.
 7. **Transparency**: Design for visibility to make debugging easier.
 8. **Robustness**: Robustness is the child of transparency and simplicity.
 9. **Representation**: Fold knowledge into data so programs can be stupid and robust.
 10. **Least Surprise**: In interface design, always do the least surprising thing.
 11. **Silence**: When a program has nothing surprising to say, it should say nothing.
 12. **Repair**: When you must fail, fail noisily and as soon as possible.
 13. **Economy**: Programmer time is expensive; conserve it in preference to machine time.
 14. **Generation**: Avoid hand-hacking; write programs to write programs when you can.
 15. **Optimization**: Prototype before polishing. Get it working before you optimize it.
 16. **Diversity**: Distrust all claims for "one true way".
 17. **Extensibility**: Design for the future, because it will be here sooner than you think.

If you're new to Unix, these principles are worth some meditation. Software-engineering texts recommend most of them; but most other operating systems lack the right tools and traditions to turn them into practice, so most programmers can't apply them with any consistency. They come to accept blunt tools, bad designs, overwork, and bloated code as normal - and then wonder what Unix fans are so annoyed about.

> Taken from The Art of UNIX Programming, by Eric
> S. Raymond. <http://www.faqs.org/docs/artu/>

> Flyer version by The Anarcat
> <http://redmine.koumbit.net/projects/unix-art-flyer>

<!-- HTML5 inline stylesheet -->
<style type="text/css">
h1 { text-align: center; }
ol { font-size: larger; }
p { font-size: normal; }
blockquote { font-size: small; font-style: italic; }
</style>
<!-- this should work in most browsers, except that markdown renders -->
<!-- to XHTML 1 by default, not HTML5 -->
